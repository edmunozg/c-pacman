#ifndef GHOST_H
#define GHOST_H

class Ghost{
private:
    BITMAP *bmPhantom;
    BITMAP *bmBufferPhantom;
    int fx ;//Posicion en x de pacman
    int fy ;//Posicion en y de pacman
    int fdir ;//Dirección del fantasma

public:
    Ghost(int x,int y);//Constructor
    void drawPhantom();
    void movePhantom();
    void touchPacman();    
};
#endif