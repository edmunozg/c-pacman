#include "ghost.h"


Ghost::Ghost(int x,int y){
    fx = x;
    fy = y;
    fdir = rand() % 4;
    bmBufferPhantom = create_bitmap(32,32);
    bmPhantom = load_bitmap("phantom.bmp",NULL);
}
/*
void Ghost::touchPacman(){
    if(py == fy && px == fx || fy == antpy && fx == antpx){
        for(int i=0; i<=5; i++){
            clear(bmBufferPacman);
            clear(bmScreen);
            drawBoard();
            blit(bmGameOver,bmBufferPacman,i*32,0,0,0,32,32);
            draw_sprite(bmScreen,bmBufferPacman,px,py);
            pantalla();
            rest(80);
        }
        px = 32 * 1;
        py = 32 * 1;
        dir = 4;
    }
}
*/
void Ghost::drawPhantom(){
	blit(bmPhantom,bmBufferPhantom,0,0,0,0,32,32);
	draw_sprite(bmScreen,bmPhantom,fx,fy);
}

void Ghost::movePhantom(){
    drawPhantom();
    touchPacman();
    int tmpMuroF;

    if(board[fy/32][(fx-32)/32] == '-'){
        fdir = rand() % 4;
    }
    if(fdir == 0){
        tmpMuroF = board[fy/32][(fx-32)/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fx -= 32;
    }
    if(fdir == 1){
        tmpMuroF = board[fy/32][(fx+32)/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fx += 32;
    }
    if(fdir == 2){
        tmpMuroF = board[(fy-32)/32][fx/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fy -= 32;
    }
    if(fdir == 3){
        tmpMuroF = board[(fy+32)/32][fx/32];
        if(tmpMuroF == '*' || tmpMuroF == 'a' || tmpMuroF == 'b'
            || tmpMuroF == 'c' || tmpMuroF == 'd' || tmpMuroF == '+')
            fdir = rand()%4;
        else
            fy += 32;
    }

    if(fx <= -32){
        fx = 870;
    }
    else if (fx >= 870){
        fx = -32;
    }
}